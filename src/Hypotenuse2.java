import java.util.Scanner;

public class Hypotenuse2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a: ");
        double a = in.nextInt();

        System.out.print("Enter b: ");
        double b = in.nextInt();

        double c;

        if (a > 0 && b > 0) {
            c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
            System.out.println(c);
        } else {
            if (a <= 0 && b <= 0) {
                System.out.println("Invalid digit!");
            } else {
                if (a <= 0 || b <= 0) {
                    System.out.println("Invalid digit!");
                }
            }
        }
    }
}
